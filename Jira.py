import requests
import json
import base64

def executeRequest(apiPath):
    cred =  "Basic " + base64.b64encode(b'userJira:tokenJira').decode("utf-8") 

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization" : cred
    }

    url = "https://mandic.atlassian.net/rest/api" + apiPath
    
    response = requests.request(
        "GET", 
        url,
        headers=headers
    )

    if "\"errorMessages\":[\"" in response.text:
        print("URL: " + url + "Response: ")
        print(response.text)

    return json.loads(response.text)

def searchIssues(jql):
    issuesJson = []
    harMoreIssues = True
    startAt = 0
    while harMoreIssues:
        request = "/3/search?jql=" + jql + "&startAt=" + str(startAt)
        responseJson = executeRequest(request)
                
        if not responseJson["issues"]:
            harMoreIssues = False
        else:
            for item in responseJson["issues"]:
                issuesJson.append(item)
        startAt = startAt + 50
    
    return issuesJson

def searchIssuesFromProjectKey(projetcKey):
    return searchIssues("project=" + projetcKey)

def searchChangelogs(issueKey):
    return executeRequest("/2/issue/" + issueKey + "/changelog")