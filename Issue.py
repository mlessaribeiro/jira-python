from Jira import searchChangelogs
from StatusTransition import StatusTransition

class Issue:

    def __init__(self, issue):
        self.issue = issue
        self.changeLogs = []

    def getIssueKey(self):
        return self.issue["key"]

    def getServiceClass(self):
        return "Normal"

    def getType(self):
        return "Valor"

    def getInitialDate(self):
        return self.getTransitionDateFromStatus("TODO")

    def getFinalDate(self):
        return self.getStatusWhenTransitTo("Done").createdDate

    def getTransitionDateFromStatus(self, status):
        return self.getStatusWhenTransitFrom(status).createdDate

    def getAllChangeLog(self):
        issueKey = self.getIssueKey()
        if not self.changeLogs:
            self.changeLogs = searchChangelogs(issueKey)
        
        return self.changeLogs

    def getAllStatusTransitions(self):
        changeLogs = self.getAllChangeLog()
        statusTransitions = []
        for changeLog in changeLogs["values"]:
            for transition in changeLog["items"]:
                if transition["field"] == "status":
                    statusTransitions.append(StatusTransition(self.getDateSubstring(changeLog["created"] + ""), transition["fromString"], transition["toString"], changeLog["author"]["displayName"]))
        return statusTransitions

    def getDateSubstring(self, dateString):
        return dateString[:10]

    def getStatusWhenTransitFrom(self, status):
        for statusTransition in self.getAllStatusTransitions():
            if (statusTransition.fromStatus == status):
                return statusTransition

        return self.statusTransitionErrorDontFindStatus(status)

    def getStatusWhenTransitTo(self, status):
        for statusTransition in self.getAllStatusTransitions():
            if (statusTransition.toStatus == status):
                return statusTransition

        return self.statusTransitionErrorDontFindStatus(status)
    
    def statusTransitionErrorDontFindStatus(self, status):
        messageError = "ERROR - don't find the status: " + status
        return StatusTransition(messageError, messageError, messageError, messageError)

    def getTransitionInformations(self):
        return self.getIssueKey() + ";" +  self.getType() + ";" + self.getServiceClass() + ";" + self.getInitialDate() + ";" + self.getFinalDate()

    def getIssuesInStatusFrom(self, issueStatus, author):
        statusTransitions = []
        for statusTransition in self.getAllStatusTransitions():
            if ((statusTransition.author == author or statusTransition.author == "Marcelo Lessa") and statusTransition.toStatus == issueStatus):
                statusTransitions.append(statusTransition)
        
        return statusTransitions