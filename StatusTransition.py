class StatusTransition:

    def __init__(self, createdDate, fromStatus, toStatus, author):
        self.createdDate = createdDate
        self.fromStatus = fromStatus
        self.toStatus = toStatus
        self.author = author