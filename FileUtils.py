
import Constant

class FileUtils:

    def writeFile(self, fileName, fileContent):
        newFile = open(fileName, Constant.CREATE_FILE)
        newFile.write(fileContent)
        newFile.close()