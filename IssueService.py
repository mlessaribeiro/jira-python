from Jira import searchIssues
from Issue import Issue
from FileUtils import FileUtils
import Constant

class IssueService:

    def generateFileIssueTransitionInformations(self, jqlFilter, fileName):
        researchedIssues = searchIssues(jqlFilter + Constant.JQL_ISSUE_STATUS_DONE)
        fileUtils = FileUtils()
        fileUtils.writeFile(fileName, self.generateFileContent(researchedIssues))

    def generateFileContent(self, researchedIssues):
        fileContent = Constant.FILE_HEADER
        for item in researchedIssues:
            issue = Issue(item)
            fileContent = fileContent + Constant.JUMP_LINE + issue.getTransitionInformations()
        
        return fileContent