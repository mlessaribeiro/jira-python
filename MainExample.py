# -*- coding: utf-8 -*-

from IssueService import IssueService
from Issue import Issue
from FileUtils import FileUtils
import Constant

jql = "resolved >= 2021-01-01 AND resolved <= 2021-03-30 AND project = DBK AND issuetype not in (Epic, Reunião, Spot, \"Sustentacao  8x5\", \"Sustentacao 24x7\")"
IssueService().generateFileIssueTransitionInformations(jql, "jiraExampleTotal.csv")

# {
#     u'startAt': 0, 
#     u'self': 
#     u'https://mandic.atlassian.net/rest/api/2/issue/DBK-861/changelog?maxResults=100&startAt=0', 
#     u'maxResults': 100, 
#     u'values': [
#         {
#             u'items': [
#                 {
#                     u'tmpToAccountId': 
#                     u'5c05738d10c30e4ac8c8cb8d', 
#                     u'from': None, 
#                     u'to': 
#                     u'5c05738d10c30e4ac8c8cb8d', 
#                     u'fromString': None, 
#                     u'field': u'assignee', 
#                     u'toString': 
#                     u'Marcelo Lessa', 
#                     u'fieldtype': 
#                     u'jira', 
#                     u'fieldId': 
#                     u'assignee', 
#                     u'tmpFromAccountId': None
#                 }, 
#                 {
#                     u'tmpToAccountId': 
#                     u'5c05738d10c30e4ac8c8cb8d', 
#                     u'from': None, 
#                     u'to': 
#                     u'5c05738d10c30e4ac8c8cb8d', 
#                     u'fromString': None, 
#                     u'field': 
#                     u'An\xe1lise t\xe9cnica', 
#                     u'toString': 
#                     u'Marcelo Lessa', 
#                     u'fieldtype': 
#                     u'custom', 
#                     u'fieldId': 
#                     u'customfield_10274', 
#                     u'tmpFromAccountId': None
#                 }, 
#                 {
#                     u'from': 
#                     u'10266', 
#                     u'to': 
#                     u'10301', 
#                     u'fromString': 
#                     u'Pool', 
#                     u'field': 
#                     u'status', 
#                     u'toString': 
#                     u'Technical Analysis', 
#                     u'fieldtype': 
#                     u'jira', 
#                     u'fieldId': 
#                     u'status'
#                 }],
#             u'created': 
#             u'2021-03-19T15:46:42.119-0300', 
#             u'id': 
#             u'3423217', 
#             u'author': {
#                 u'displayName': 
#                 u'Marcelo Lessa', 
#                 u'self': 
#                 u'https://mandic.atlassian.net/rest/api/2/user?accountId=5c05738d10c30e4ac8c8cb8d', 
#                 u'avatarUrls': {
#                     u'24x24': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/24', 
#                     u'32x32': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/32', 
#                     u'48x48': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/48', 
#                     u'16x16': u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/16'
#                 }, 
#                 u'accountType': 
#                 u'atlassian', 
#                 u'active': True, 
#                 u'timeZone': 
#                 u'America/Sao_Paulo', 
#                 u'accountId': 
#                 u'5c05738d10c30e4ac8c8cb8d'
#             }
#         }, 
#         {
#             u'items': [
#                 {
#                     u'from': 
#                     u'10301', 
#                     u'to': 
#                     u'10275', 
#                     u'fromString': 
#                     u'Technical Analysis', 
#                     u'field': u'status', 
#                     u'toString': 
#                     u'Ready for Development', 
#                     u'fieldtype': 
#                     u'jira', 
#                     u'fieldId': 
#                     u'status'
#                 }
#             ],
#             u'created': 
#             u'2021-03-19T15:58:38.664-0300', 
#             u'id': 
#             u'3423237', 
#             u'author': {
#                 u'displayName': 
#                 u'Marcelo Lessa', 
#                 u'self': 
#                 u'https://mandic.atlassian.net/rest/api/2/user?accountId=5c05738d10c30e4ac8c8cb8d', 
#                 u'avatarUrls': {
#                     u'24x24': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/24', 
#                     u'32x32': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/32', 
#                     u'48x48': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/48', 
#                     u'16x16': 
#                     u'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5c05738d10c30e4ac8c8cb8d/e8a405a8-a577-4997-b840-900542b0c4bc/16'
#                 }, 
#                 u'accountType': 
#                 u'atlassian', 
#                 u'active': 
#                 True, 
#                 u'timeZone': 
#                 u'America/Sao_Paulo', 
#                 u'accountId': 
#                 u'5c05738d10c30e4ac8c8cb8d'
#             }
#         }
#     ], 
#     u'total': 2, 
#     u'isLast': True
# }





# {
#   "id": "10002",
#   "self": "https://your-domain.atlassian.net/rest/api/3/issue/10002",
#   "key": "ED-1",
#   "fields": {
#     "watcher": {
#       "self": "https://your-domain.atlassian.net/rest/api/3/issue/EX-1/watchers",
#       "isWatching": false,
#       "watchCount": 1,
#       "watchers": [
#         {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "accountId": "5b10a2844c20165700ede21g",
#           "displayName": "Mia Krystof",
#           "active": false
#         }
#       ]
#     },
#     "attachment": [
#       {
#         "id": 10000,
#         "self": "https://your-domain.atlassian.net/rest/api/3/attachments/10000",
#         "filename": "picture.jpg",
#         "author": {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "key": "",
#           "accountId": "5b10a2844c20165700ede21g",
#           "name": "",
#           "avatarUrls": {
#             "48x48": "https://avatar-management--avatars.server-location.prod.public.atl-paas.net/initials/MK-5.png?size=48&s=48",
#             "24x24": "https://avatar-management--avatars.server-location.prod.public.atl-paas.net/initials/MK-5.png?size=24&s=24",
#             "16x16": "https://avatar-management--avatars.server-location.prod.public.atl-paas.net/initials/MK-5.png?size=16&s=16",
#             "32x32": "https://avatar-management--avatars.server-location.prod.public.atl-paas.net/initials/MK-5.png?size=32&s=32"
#           },
#           "displayName": "Mia Krystof",
#           "active": false
#         },
#         "created": "2021-03-24T02:52:34.947+0000",
#         "size": 23123,
#         "mimeType": "image/jpeg",
#         "content": "https://your-domain.atlassian.net/jira/secure/attachments/10000/picture.jpg",
#         "thumbnail": "https://your-domain.atlassian.net/jira/secure/thumbnail/10000/picture.jpg"
#       }
#     ],
#     "sub-tasks": [
#       {
#         "id": "10000",
#         "type": {
#           "id": "10000",
#           "name": "",
#           "inward": "Parent",
#           "outward": "Sub-task"
#         },
#         "outwardIssue": {
#           "id": "10003",
#           "key": "ED-2",
#           "self": "https://your-domain.atlassian.net/rest/api/3/issue/ED-2",
#           "fields": {
#             "status": {
#               "iconUrl": "https://your-domain.atlassian.net/images/icons/statuses/open.png",
#               "name": "Open"
#             }
#           }
#         }
#       }
#     ],
#     "description": {
#       "type": "doc",
#       "version": 1,
#       "content": [
#         {
#           "type": "paragraph",
#           "content": [
#             {
#               "type": "text",
#               "text": "Main order flow broken"
#             }
#           ]
#         }
#       ]
#     },
#     "project": {
#       "self": "https://your-domain.atlassian.net/rest/api/3/project/EX",
#       "id": "10000",
#       "key": "EX",
#       "name": "Example",
#       "avatarUrls": {
#         "48x48": "https://your-domain.atlassian.net/secure/projectavatar?size=large&pid=10000",
#         "24x24": "https://your-domain.atlassian.net/secure/projectavatar?size=small&pid=10000",
#         "16x16": "https://your-domain.atlassian.net/secure/projectavatar?size=xsmall&pid=10000",
#         "32x32": "https://your-domain.atlassian.net/secure/projectavatar?size=medium&pid=10000"
#       },
#       "projectCategory": {
#         "self": "https://your-domain.atlassian.net/rest/api/3/projectCategory/10000",
#         "id": "10000",
#         "name": "FIRST",
#         "description": "First Project Category"
#       },
#       "simplified": false,
#       "style": "classic",
#       "insight": {
#         "totalIssueCount": 100,
#         "lastIssueUpdateTime": "2021-03-24T02:52:32.943+0000"
#       }
#     },
#     "comment": [
#       {
#         "self": "https://your-domain.atlassian.net/rest/api/3/issue/10010/comment/10000",
#         "id": "10000",
#         "author": {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "accountId": "5b10a2844c20165700ede21g",
#           "displayName": "Mia Krystof",
#           "active": false
#         },
#         "body": {
#           "type": "doc",
#           "version": 1,
#           "content": [
#             {
#               "type": "paragraph",
#               "content": [
#                 {
#                   "type": "text",
#                   "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget venenatis elit. Duis eu justo eget augue iaculis fermentum. Sed semper quam laoreet nisi egestas at posuere augue semper."
#                 }
#               ]
#             }
#           ]
#         },
#         "updateAuthor": {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "accountId": "5b10a2844c20165700ede21g",
#           "displayName": "Mia Krystof",
#           "active": false
#         },
#         "created": "2021-03-24T02:52:33.820+0000",
#         "updated": "2021-03-24T02:52:33.820+0000",
#         "visibility": {
#           "type": "role",
#           "value": "Administrators"
#         }
#       }
#     ],
#     "issuelinks": [
#       {
#         "id": "10001",
#         "type": {
#           "id": "10000",
#           "name": "Dependent",
#           "inward": "depends on",
#           "outward": "is depended by"
#         },
#         "outwardIssue": {
#           "id": "10004L",
#           "key": "PR-2",
#           "self": "https://your-domain.atlassian.net/rest/api/3/issue/PR-2",
#           "fields": {
#             "status": {
#               "iconUrl": "https://your-domain.atlassian.net/images/icons/statuses/open.png",
#               "name": "Open"
#             }
#           }
#         }
#       },
#       {
#         "id": "10002",
#         "type": {
#           "id": "10000",
#           "name": "Dependent",
#           "inward": "depends on",
#           "outward": "is depended by"
#         },
#         "inwardIssue": {
#           "id": "10004",
#           "key": "PR-3",
#           "self": "https://your-domain.atlassian.net/rest/api/3/issue/PR-3",
#           "fields": {
#             "status": {
#               "iconUrl": "https://your-domain.atlassian.net/images/icons/statuses/open.png",
#               "name": "Open"
#             }
#           }
#         }
#       }
#     ],
#     "worklog": [
#       {
#         "self": "https://your-domain.atlassian.net/rest/api/3/issue/10010/worklog/10000",
#         "author": {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "accountId": "5b10a2844c20165700ede21g",
#           "displayName": "Mia Krystof",
#           "active": false
#         },
#         "updateAuthor": {
#           "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
#           "accountId": "5b10a2844c20165700ede21g",
#           "displayName": "Mia Krystof",
#           "active": false
#         },
#         "comment": {
#           "type": "doc",
#           "version": 1,
#           "content": [
#             {
#               "type": "paragraph",
#               "content": [
#                 {
#                   "type": "text",
#                   "text": "I did some work here."
#                 }
#               ]
#             }
#           ]
#         },
#         "updated": "2021-03-24T02:52:34.062+0000",
#         "visibility": {
#           "type": "group",
#           "value": "jira-developers"
#         },
#         "started": "2021-03-24T02:52:34.062+0000",
#         "timeSpent": "3h 20m",
#         "timeSpentSeconds": 12000,
#         "id": "100028",
#         "issueId": "10002"
#       }
#     ],
#     "updated": 1,
#     "timetracking": {
#       "originalEstimate": "10m",
#       "remainingEstimate": "3m",
#       "timeSpent": "6m",
#       "originalEstimateSeconds": 600,
#       "remainingEstimateSeconds": 200,
#       "timeSpentSeconds": 400
#     }
#   }
# }